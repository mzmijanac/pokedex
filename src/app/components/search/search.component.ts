import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SearchService } from '../../services/search.service';
import { PokemonShort } from '../../models/pokemon-short';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css'],
  providers: [SearchService]
})

export class SearchComponent implements OnInit {
  allPokemons: any[];
  searchTerm: string;
  resultsArray: any[];
  spriteUrl: string;
  loading: boolean;

  constructor(
    private params: ActivatedRoute,
    private searchService: SearchService,
    private title: Title) {
  }

  ngOnInit() {
    this.spriteUrl = this.searchService.spriteUrl;
    this.allPokemons = [];
    this.getPokemons();
  }

  /* Ova logika bi trebala biti u Serviceu, ali cijeli ovaj search je ionako ljuti hack zbog GET-only API-ja
     Also, za ovak malen broj rezultata, nema smisla ubacivati nekakav search algoritam, eventualno bih mogao
     nekakav bisection po abecedi, pa ovisno koje slovo user upiše, da sreže rezultate u tom smjeru...
  */
  searchPokemons(searchTerm: string): string[] {
    this.resultsArray = [];
    this.loading = true;
    this.searchTerm = searchTerm;
    this.title.setTitle(`Pokedex | Search results for` + searchTerm);
    let term = searchTerm.toLowerCase();
    /* Moram loopati sa klasičnom for petljom umjesto forOf jer mi treba index pokemona,
      a ne želimo raditi joinanje 2 resourcea za svih 800+ pokemona jer nije fer prema API-ju,
      Inače bi bilo ovak simple:
      for(let pokemon of this.allPokemons){
        if(pokemon.name.indexOf(term) >= 0){
          this.resultsArray.push(pokemon);
        }
      }
    */
    for(let i = 0; i < this.allPokemons.length; i++) {
      if(this.allPokemons[i].name.indexOf(term) >= 0){
        this.resultsArray.push({
          id: i+1, // jerbo će mi slike offsetat za 0, pošto je prvi pokemon Bulbasaur sa index 1, a ne 0
          name: this.searchService.firstLetterToUppercase(this.allPokemons[i].name),
        })
      }
    }
    this.loading = false;
    return this.resultsArray;
  }

  // Samo grabimo sva pokemon imena sa API-ja, da je što brže, jer nam za search treba samo ime ionako
  getPokemons(){
    this.searchService.fetchAllPokemons()
                      .subscribe(data => this.allPokemons = data['results']);
  }
}
