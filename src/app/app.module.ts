// Modules
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http'; // Za probu u SearchServiceu
import { AppRoutingModule } from './modules/app-routing.module';

// Vendor Modules
import { ChartsModule } from 'ng2-charts/ng2-charts';

// Components
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { FavoritesComponent } from './components/favorites/favorites.component';
import { SearchComponent } from './components/search/search.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { PokemonComponent } from './components/pokemon/pokemon/pokemon.component';
import { PokemonGridComponent } from './components/pokemon/pokemon-grid/pokemon-grid.component';
import { PokemonGridItemComponent } from './components/pokemon/pokemon-grid-item/pokemon-grid-item.component';
import { PokemonDescriptionComponent } from './components/pokemon/pokemon-details/pokemon-description/pokemon-description.component';
import { PokemonStatsComponent } from './components/pokemon/pokemon-details/pokemon-stats/pokemon-stats.component';
import { PokemonAbilitiesComponent } from './components/pokemon/pokemon-details/pokemon-abilities/pokemon-abilities.component';
import { PokemonMovesComponent } from './components/pokemon/pokemon-details/pokemon-moves/pokemon-moves.component';
import { NavigationComponent } from './components/navigation/navigation.component';
import { PaginationComponent } from './components/pagination/pagination.component';

// Services
import { PokemonDataService } from './services/pokemon-data.service';
import { FavoritesControlComponent } from './components/favorites/favorites-control/favorites-control.component';
import { LoadingComponent } from './components/loading/loading.component';
import { SearchFieldComponent } from './components/search/search-field/search-field.component';



@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    FavoritesComponent,
    SearchComponent,
    PageNotFoundComponent,
    PokemonGridComponent,
    PokemonGridItemComponent,
    PokemonDescriptionComponent,
    PokemonStatsComponent,
    PokemonAbilitiesComponent,
    PokemonMovesComponent,
    NavigationComponent,
    PaginationComponent,
    PokemonComponent,
    FavoritesControlComponent,
    LoadingComponent,
    SearchFieldComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    AppRoutingModule,
    HttpClientModule,
    ChartsModule
  ],
  providers: [
    PokemonDataService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
