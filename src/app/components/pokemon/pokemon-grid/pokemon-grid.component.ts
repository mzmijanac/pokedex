import { Component, OnInit } from '@angular/core';
import { PokemonDataService } from '../../../services/pokemon-data.service';
import { PokemonShort } from '../../../models/pokemon-short';

@Component({
  selector: 'app-pokemon-grid',
  templateUrl: './pokemon-grid.component.html',
  styleUrls: ['./pokemon-grid.component.css']
})
export class PokemonGridComponent implements OnInit {
  pokemons: PokemonShort[];
  count: number = 0;
  offset: number = 0;
  limit: number = 20;
  loading: boolean = false;

  constructor(private pokemonService: PokemonDataService) { }

  ngOnInit() {
    this.fetchAllPokemons(this.offset, this.limit);
  }

  fetchAllPokemons(offset: number, limit: number){
    this.loading = true;
    // Initializiram pokemon array ovdje zbog paginationa, jer ne želim kod promjene offseta zadržati "stare" pokemone u arrayu
    this.pokemons = [];
    this.pokemonService.fetchAllPokemons(offset, limit).subscribe(res => {
      this.pokemons = res.pokemons;
      this.count = res.count;
      this.loading = false;
    }, () => this.loading = false); // fejk error callback da završi loading
  }

  // Tu primimo offset koji smo emittali iz pagination komponente, i invokaemo fetchAllPokemons() ponovno
  onPageChange(offset){
    this.offset = offset;
    this.fetchAllPokemons(offset, this.limit);
  }
}
