export class PokemonTypes {
  slot: number;
  name: string;


  constructor(slot: number, name: string){
    this.slot = slot;
    this.name = name;
  }
}
