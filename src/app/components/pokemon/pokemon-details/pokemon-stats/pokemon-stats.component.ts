import { Component, OnInit, Input } from '@angular/core';
import { PokemonStats } from '../../../../models/pokemon-stats';

@Component({
  selector: 'app-pokemon-stats',
  templateUrl: './pokemon-stats.component.html',
  styleUrls: ['./pokemon-stats.component.css']
})
export class PokemonStatsComponent implements OnInit {
  @Input() stats: PokemonStats;

  hp: number;
  attack: number;
  defense: number;
  specialAttack: number;
  specialDefense: number;
  speed: number;

  public statsChartLabels: string[] = ['HP', 'ATK', 'DEF', 'SPA', 'SPD', 'SPE'];
  public statsChartData: any[];
  public statsChartOptions: any = {
    scaleOverride: true,
    scaleSteps: 5,
    scaleStepWidth: 20,
    scaleStartValue: 100,
    responsive: true,
    maintainAspectRadio: true,
    scale: {
      ticks: {
        beginAtZero: false,
        min: 0,
        max: 100,
        stepSize: 20
      },
      pointLabels: {
        fontSize: 18
      }
    },
    legend: {
      display: false
    }
  };

  public statsChartType: string = 'radar';

  constructor() { }

  ngOnInit() {
    this.hp = this.stats.hp;
    this.attack = this.stats.attack;
    this.defense = this.stats.defense;
    this.specialAttack = this.stats.specialAttack;
    this.specialDefense = this.stats.specialDefense;
    this.speed = this.stats.speed;
    this.statsChartData = [
      this.hp,
      this.attack,
      this.defense,
      this.specialAttack,
      this.specialDefense,
      this.speed
    ]
  }
}
