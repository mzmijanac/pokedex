import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-page-not-found',
  templateUrl: './page-not-found.component.html',
  styleUrls: ['./page-not-found.component.css']
})
export class PageNotFoundComponent implements OnInit {

  constructor(
    private location: Location,
    private title: Title) { }

  ngOnInit() {
    this.title.setTitle(`Pokedex | 404`);
  }

  // Jednostavna "go back" navigacija, moram provjeriti kak se ponaša state
  goBack(): void {
    this.location.back();
  }

}
