import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FavoritesControlComponent } from './favorites-control.component';

describe('FavoritesControlComponent', () => {
  let component: FavoritesControlComponent;
  let fixture: ComponentFixture<FavoritesControlComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FavoritesControlComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FavoritesControlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
