import { Component, OnInit } from '@angular/core';
import { PokemonDataService } from '../../services/pokemon-data.service';
import { Pokemon } from '../../models/pokemon';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-favorites',
  templateUrl: './favorites.component.html',
  styleUrls: ['./favorites.component.css']
})
export class FavoritesComponent implements OnInit {
  favorites: Pokemon[];


  constructor(
    private pokemonService: PokemonDataService,
    private title: Title) { }

  ngOnInit() {
    this.title.setTitle(`Pokedex | Favorites`);
    this.updateFavorites();
  }

  updateFavorites(){
    this.favorites = [];
    this.favorites = JSON.parse(localStorage.getItem('favorites'));
  }
}
