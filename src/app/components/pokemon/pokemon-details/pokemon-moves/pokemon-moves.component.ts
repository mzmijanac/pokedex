import { Component, OnInit, Input } from '@angular/core';
import { PokemonMoves } from '../../../../models/pokemon-moves';

@Component({
  selector: 'app-pokemon-moves',
  templateUrl: './pokemon-moves.component.html',
  styleUrls: ['./pokemon-moves.component.css']
})
export class PokemonMovesComponent implements OnInit {
  @Input() moves: PokemonMoves;

  constructor() { }

  ngOnInit() {
  }

}
