import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { PokemonDataService } from '../../../services/pokemon-data.service';
import { Pokemon } from '../../../models/pokemon';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-pokemon',
  templateUrl: './pokemon.component.html',
  styleUrls: ['./pokemon.component.css']
})
export class PokemonComponent implements OnInit {
  pokemon: Pokemon;
  loading: boolean = false;
  artworkUrl: string = 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other-sprites/official-artwork/';

  constructor(
    private pokemonService: PokemonDataService,
    private location: Location,
    private route: ActivatedRoute,
    private title: Title ) {

  }

  ngOnInit() {
    // Koristimo params i mapiramo ga na "id" pomoću RxJS map()-e, zatim koristimo fetchPokemon() iz servicea,
    // no kako fetchPokemon() returna Observable, koristimo flatMap() kako bi izbjegli Observable unutar Observablea,
    // te konačno rezultat spremimo u pokemon varijablu.
    /* this.route.params.map(params => params['id'])
      .flatMap(id => {
        this.artworkUrl += `${this.artworkUrl}${id}.png`;
        return this.pokemonService.fetchPokemon(id)})
      .subscribe(pokemon => {this.pokemon = pokemon}); */

    // Updated verzija, jer sam htio dodati i promjenu page Titlea:
    let observable = this.route.params.map(params => params['id'])
                                      .flatMap(id => this.pokemonService.fetchPokemon(id))
                                      .share(); // da se izbjegne double call

    this.loading = true;

    observable.subscribe(pokemon => {
      this.artworkUrl += `${this.artworkUrl}${pokemon.description.id}.png`;
      this.loading = false;
      return this.pokemon = pokemon}, () => this.loading = false ); // fejk callback da završi loading

    observable.subscribe(pokemon => this.title.setTitle(`Pokedex | ${pokemon.description.name}`));
  }

  // Jednostavna "go back" navigacija, moram provjeriti kak se ponaša state
  goBack(): void {
    this.location.back();
  }
}
