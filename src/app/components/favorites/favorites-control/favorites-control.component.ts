import { Component, OnInit, Input } from '@angular/core';
import { PokemonDataService } from '../../../services/pokemon-data.service';
import { Pokemon } from '../../../models/pokemon';

@Component({
  selector: 'app-favorites-control',
  templateUrl: './favorites-control.component.html',
  styleUrls: ['./favorites-control.component.css']
})
export class FavoritesControlComponent implements OnInit {
  @Input() pokemon: Pokemon;

  constructor(private pokemonService: PokemonDataService) { }

  ngOnInit() { }

  addToFavorites(pokemon: Pokemon){
    this.pokemonService.addToFavorites(pokemon);
  }

  removeFromFavorites(pokemon: Pokemon){
    this.pokemon.isFavorite = false;
    this.pokemonService.removeFromFavorites(pokemon);
  }

}
