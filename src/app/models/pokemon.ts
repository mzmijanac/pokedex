import { PokemonAbilities } from './pokemon-abilities';
import { PokemonDescription } from './pokemon-description';
import { PokemonMoves } from './pokemon-moves';
import { PokemonStats } from './pokemon-stats';

export class Pokemon {
  abilities?: PokemonAbilities[];
  description: PokemonDescription;
  moves?: PokemonMoves[];
  stats?: PokemonStats;
  isFavorite?: boolean;

  constructor (
    description: PokemonDescription,
    stats?: PokemonStats,
    abilities?: PokemonAbilities[],
    moves?: PokemonMoves[],
    isFavorite?: boolean,
  ) {
    this.description = description;
    this.stats = stats;
    this.abilities = abilities;
    this.moves = moves;
    this.isFavorite = isFavorite;
  }
}

