import { Component, OnInit, Input } from '@angular/core';
import { PokemonAbilities } from '../../../../models/pokemon-abilities';

@Component({
  selector: 'app-pokemon-abilities',
  templateUrl: './pokemon-abilities.component.html',
  styleUrls: ['./pokemon-abilities.component.css']
})
export class PokemonAbilitiesComponent implements OnInit {
  @Input() abilities: PokemonAbilities[];


  constructor() { }
  ngOnInit() {
  }
}
