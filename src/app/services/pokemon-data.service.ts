import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
// Tu bih trebao koristiti novi HttpClientModule, ali onda moram kompletno razjebati cijelu Observable logiku, maybe next time,
// no dobra je fora jer više ne moraš manually extractati JSON i RxJS mapirati Response Object
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';

// Models
import { Pokemon } from '../models/pokemon';
import { PokemonAbilities } from '../models/pokemon-abilities';
import { PokemonDescription } from '../models/pokemon-description';
import { PokemonList } from '../models/pokemon-list';
import { PokemonMoves } from '../models/pokemon-moves';
import { PokemonStats } from '../models/pokemon-stats';
import { PokemonShort } from '../models/pokemon-short'; // For grid list, favorites & search
import { PokemonTypes } from '../models/pokemon-types';


@Injectable()
export class PokemonDataService {
  // Configuration variables
  // Da se tu nalazi neki API key, ne bih ga storeo ovdje već u neki externi config file, pa povlačio od tamo, a file naravno u .gitignore
  private apiUrl: string = 'https://pokeapi.co/api/v2/pokemon';
  private spriteUrl: string = 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon';
  private pokedexUrl: string = 'http://pokeapi.co/api/v2/pokedex/1';
  private speciesUrl: string = 'http://pokeapi.co/api/v2/pokemon-species';

  // Ovo extractanje ID-a može biti i drugačije napravljeno, npr funkcija koja returna parseInt(url.split('/')[6]);
  // private extractIdRegex = /^https:\/\/pokeapi.co\/api\/v2\/pokemon\/(\d+)\/$/;

  // API parameters
  /*   Nakon nekog vremena drkanja po API-ju saznaš da nisu apsolutno svi pokemoni pokemoni, već da postoje neke mega evolucije i pičke materine,
  a da je stvarni broj pokemona zapravo 721. U prvoj varijanti sam count povlačio preko API-ja (broj entrya), pa bih iz njega
  izvlačio broj pageova za paginaciju. Bejzikli, ako želim 20 rezultata po pageu, nakon fetcha:

    data => {
      this.limit = data.count;
      this.count = data.count - (data.count % this.offsetIncrement);
      this.lastPage = this.count / this.offsetIncrement;
    } ...

  A nakon toga bih fetchao pokemone za svaki page pomoću offseta:
    this.http.get(`${this.apiUrl}pokemon/${offset >= 0 ? this.offsetSuffix : ''}${offset}`).subscribe( ... )

  Za firstPage bi this.offset = 0, a za lastPage this.offset = this.count.

  Za next page this.offset += this.offsetIncrement, te za previousPage:
    if(this.offset >= this.offsetIncrement){
      this.offset -= this.offsetIncrement;
      ...
    }
  No kako je API imao užasan response (~2-4s po requestu), odustao sam od te ideje.
  Alternativni pristup je trenutno live, vidljiv i objašnjen u "components/pagination/pagination.component.ts"
  */
  private offset: number;
  private offsetIncrement: number;
  private limit: number;
  // private count: number; // Inače 811 i fetchao sam ga kak je gore opisano

  // Inicijalno niti jedan pokemon nije u favoritesima
  public isFavorite: boolean = false;

  constructor(private http: Http) { }

  // Fetch from API
  // Ovdje jednostavno grabim sve pokemone, te koristim default vrijednosti za offset i limit, a funkcija vraća Observable po modelu PokemonList
  public fetchAllPokemons(offset: number = 0, limit: number = 20): Observable<PokemonList> {
    return this.http.get(`${this.apiUrl}?offset=${offset}&limit=${limit}`)
                    .map(res => res.json())
                    .map(results => this.getAllPokemons(results));
  }

  // Kasnije sam pronašao i pokedex endpoint na API-ju, koji je već filtriran od mega evolucija i sortiran po tom nekom National indexu,
  // te je fetchAll možda mogao biti iskorišten na drugi način:
  /*
    fetchAllPokemons(){
      return this.http.get(${this.pokedexUrl})
        .map(res => res.json())
        ... pa mapirati po pokemon_entries, jer su navodno pokemon name i pokemon species isti kurac, oh well...
    }
  */

  // Ovdje grabim individualnog pokemona prema id-ju, također vraćam Observable po modelu Pokemon, a usput mapiram reponseove po svim ostalim modelima,
  // te za svaki response radim novu instancu Pokemon classe. Probably nije najbolji solution, možda sam mogao koristiti i ReplaySubject<Response>
  // i za sve i za individualne pokemone, pa onda iz memorije emitati Observable, te ukoliko Pokemon već ne postoji u memoriji, tek onda fetchati.
  // Oh well, limiti single threaded JS-a i sporog API-ja... deal with it. :-\
  public fetchPokemon(id: number): Observable<Pokemon> {
    // Observables are cool :D
    // http://reactivex.io/rxjs/class/es6/Observable.js~Observable.html#static-method-forkJoin
    // Ovdje želimo povezati podatke iz dva različita resourcea, odnosno "fork"-ea preko ID-ja, pretty neat trick
    return Observable.forkJoin(
      this.http.get(`${this.apiUrl}/${id}/`).map(res => res.json()),
      this.http.get(`${this.speciesUrl}/${id}/`).map(res => res.json())
    ).map(data => new Pokemon(
      new PokemonDescription(
        data[0].id, // ID
        this.firstLetterToUppercase(data[0].name), // Name
        `${this.spriteUrl}/${data[0].id}.png`, // Sprite
        parseInt(data[0].height),// Height
        parseInt(data[0].weight), // Weight
        this.getInfoText(data[1]['flavor_text_entries'])[0], // Infotext
        data[1].color.name, // Color
        this.getTypes(data[0].types) // PokemonTypes[]
     ), // end PokemonDescription
        this.getStats(data[0].stats), // PokemonStats
        this.getAbilities(data[0].abilities), // PokemonAbilities[]
        this.getMoves(data[0].moves), // PokemonMoves[]
        this.checkFavorite(data[0].name))); // PokemonFavorites
  }

  private getAllPokemons(data): PokemonList {
    // Treba filtrirati pokemone, jer ovi sa ID preko 10000 nisu pravi nego te nekakve mega evolucije, kažu, vele
    // Ovaj step je optional, ali jebiga, OCD jer ove mega evolucije najčešće nemaju sprite i onda mi kompliciraju layout logiku
    let results = data.results.map(result => this.getPokemon(result))
                              .filter(pokemon => pokemon.id < 10000);
    // Zatim kad smo izbacili evolucije, treba smanjiti count sa 811 na 721
    return new PokemonList(results, 721);
  }

  private getPokemon(data){
    // Izvadimo ID iz URL-a, ako postoji, ako ne, onda null, pomoću ternary operatora, alternativno je izvedivo i pomoću RegExp-a
    let urlParse = data.url.split('/')[6],
        id = urlParse == null ? null : parseInt(urlParse),
        sprite = id == null ? null : `${this.spriteUrl}/${id}.png`;
    return new PokemonShort(id, this.firstLetterToUppercase(data.name), sprite);
  }

  private getTypes(types: any[]): PokemonTypes[] {
    return types.map(type => new PokemonTypes(type.slot, this.firstLetterToUppercase(type.type.name)))
                .sort((type1, type2) => type1.slot - type2.slot);
  }

  private getStats(stats: any[]): PokemonStats {
    return new PokemonStats(
      stats.find(stat => stat.stat.name === 'hp')['base_stat'],
      stats.find(stat => stat.stat.name === 'attack')['base_stat'],
      stats.find(stat => stat.stat.name === 'defense')['base_stat'],
      stats.find(stat => stat.stat.name === 'special-attack')['base_stat'],
      stats.find(stat => stat.stat.name === 'special-defense')['base_stat'],
      stats.find(stat => stat.stat.name === 'speed')['base_stat']
    );
  }

  private getInfoText(entries: any[]): string[] {
    // Hardcoded, možda bih mogao gore negdje dodati opciju izbora jezika, ali je ionako cijeli app na engleskom, pa...
    return entries.filter(entry => entry.language.name === 'en')
                  .map(entry => entry['flavor_text']);
  }

  private getMoves(moves: any[]): PokemonMoves[] {
    return moves.map(move => new PokemonMoves(this.firstLetterToUppercase(move.move.name)));
  }

  private getAbilities(abilities: any[]): PokemonAbilities[] {
    return abilities.map(ability => new PokemonAbilities(
                          this.firstLetterToUppercase(ability.ability.name),
                          ability['is_hidden'], ability.slot))
                    .sort((ability1, ability2) => ability1.order - ability2.order);
  }

  // Handleanje favoritesa sa localStorageom, pretty straight forward
  public addToFavorites(pokemon: Pokemon): Pokemon {
    if(!pokemon.isFavorite) {
      // promjeni state pokemonu
      pokemon.isFavorite = true;
      let favorites;
      // Ako je localStorage empty, initialize novi favorites array,
      // dodaj pokemona na počekat favorites arraya i napravi novi localStorage item
      if(localStorage.getItem('favorites') === null){
        favorites = [];
        favorites.unshift(pokemon);
        localStorage.setItem('favorites', JSON.stringify(favorites));
      } else {
        // Ako localStorage već postoji, onda jednostavno dodaj tog pokemona na početak i updateaj localStorage
        favorites = JSON.parse(localStorage.getItem('favorites'));
        favorites.unshift(pokemon);
        localStorage.setItem('favorites', JSON.stringify(favorites));
      }
      return pokemon;
    }
  }

  public removeFromFavorites(pokemon: Pokemon): void {
    let favorites = JSON.parse(localStorage.getItem('favorites'));
    for(let i = 0; i < favorites.length; i++){
      if(pokemon.description.id == favorites[i].description.id){
        favorites.splice(i, 1);
        localStorage.setItem('favorites', JSON.stringify(favorites));
      }
    }
  }

  private checkFavorite(name: string): boolean {
    let nameCheck = this.firstLetterToUppercase(name);
    if(localStorage.getItem('favorites') === null){
      return false;
    } else {
      let favorites = JSON.parse(localStorage.getItem('favorites'));
      for (let i = 0; i < favorites.length; i ++){
        if(nameCheck == favorites[i].description.name){
          return true;
        }
      }
    }
  }

  // Utilities, ovo je bih mogao kasnije staviti u nekakav drugi file, ovisno koliko ih se na kraju nakupi...
  // Alternativno sam mogao importati lodash, no preferiram sve ručno u ovom app-u :D
  // Alternativnije sam mogao napraviti Angular Pipe... but lazy
  public firstLetterToUppercase(string: string): string {
    return string.charAt(0).toUpperCase() + string.slice(1);
  }
}
