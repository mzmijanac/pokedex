import { PokemonDescription } from './pokemon-description';

export class PokemonList {
  pokemons: PokemonDescription[];
  count: number;

  constructor(pokemons: PokemonDescription[], count: number){
    this.pokemons = pokemons;
    this.count = count;
  }
}
