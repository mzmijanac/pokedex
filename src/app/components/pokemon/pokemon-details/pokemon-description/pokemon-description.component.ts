import { Component, OnInit, Input } from '@angular/core';
import { PokemonDescription } from '../../../../models/pokemon-description';
import { PokemonTypes } from '../../../../models/pokemon-types';

@Component({
  selector: 'app-pokemon-description',
  templateUrl: './pokemon-description.component.html',
  styleUrls: ['./pokemon-description.component.css']
})
export class PokemonDescriptionComponent implements OnInit {
  @Input() description: PokemonDescription;
  @Input() artworkUrl: string;

  id: number;
  name: string;
  sprite: string;
  height?: number;
  weight?: number;
  infoText?: string;
  color?: string;
  types?: PokemonTypes[];

  constructor() { }

  ngOnInit() {
    this.id = this.description.id;
    this.name = this.description.name;
    this.height = this.description.height;
    this.weight = this.description.weight;
    this.infoText = this.description.infoText;
    this.color = this.description.color;
    this.types = this.description.type;
  }

  // Oh well...
  typeColor(type: string): string{
    switch(type) {
      case 'Normal':
        return '#A8A77A';
      case 'Fighting':
        return '#C22E28';
      case 'Flying':
        return '#A98FF3';
      case 'Poison':
        return '#A33EA1';
      case 'Ground':
        return '#E2BF65';
      case 'Rock':
        return '#B6A136';
      case 'Bug':
        return '#A6B91A';
      case 'Ghost':
        return '#735797';
      case 'Steel':
        return '#B7B7CE';
      case 'Fire':
        return '#EE8130';
      case 'Water':
        return '#6390F0';
      case 'Grass':
        return '#7AC74C';
      case 'Electric':
        return '#F7D02C';
      case 'Psychic':
        return '#F95587';
      case 'Ice':
        return '#96D9D6';
      case 'Dragon':
        return '#6F35FC';
      case 'Fairy':
        return '#D685AD';
      case 'Dark':
        return '#705746';
      default:
        return '#333';
    }
  }

}
