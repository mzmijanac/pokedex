import { Component, OnInit, Output, EventEmitter } from '@angular/core';


@Component({
  selector: 'app-search-field',
  templateUrl: './search-field.component.html',
  styleUrls: ['./search-field.component.css'],
})
export class SearchFieldComponent implements OnInit {
  @Output() searchTerm: EventEmitter<any> = new EventEmitter<any>();
  constructor() { }

  ngOnInit() {
  }

  // Ovdje emittamo event sa text Objectom ukucanim u search field nakon što lupiš enter, te ga primamo u parent search componentu
  searchPokemons(term: string){
    this.searchTerm.emit({searchTerm: term});
  }

}
