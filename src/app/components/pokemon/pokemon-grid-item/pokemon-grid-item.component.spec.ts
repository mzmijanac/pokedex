import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PokemonGridItemComponent } from './pokemon-grid-item.component';

describe('PokemonGridItemComponent', () => {
  let component: PokemonGridItemComponent;
  let fixture: ComponentFixture<PokemonGridItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PokemonGridItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PokemonGridItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
