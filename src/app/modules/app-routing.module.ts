import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// Route Components
import { AppComponent } from '../app.component';
import { HomeComponent } from '../components/home/home.component';
import { FavoritesComponent } from '../components/favorites/favorites.component';
import { SearchComponent } from '../components/search/search.component';
import { PageNotFoundComponent } from '../components/page-not-found/page-not-found.component';
import { PokemonComponent } from '../components/pokemon/pokemon/pokemon.component';


const appRoutes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  { path: 'favorites', component: FavoritesComponent },
  { path: 'pokemon/:id', component: PokemonComponent },
  { path: 'search', component: SearchComponent },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [ RouterModule.forRoot(appRoutes) ],
  exports: [ RouterModule ]
})

export class AppRoutingModule {}
