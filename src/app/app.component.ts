import { Component, OnInit } from '@angular/core';
import { Router, NavigationStart } from '@angular/router';
import { Title } from '@angular/platform-browser';



@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  // Zbog dodavanja title changea u pokemon komponenti, potrebno je i ovdje uvesti title revert
  constructor(private title: Title, private router: Router){

  }

  ngOnInit(){
    this.router.events.filter(event => event instanceof NavigationStart)
      .subscribe(event => this.title.setTitle('Pokedex | Loading...'));
  }
}
