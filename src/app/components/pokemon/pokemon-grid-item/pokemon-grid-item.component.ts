import { Component, OnInit, Input } from '@angular/core';
import { PokemonShort } from '../../../models/pokemon-short';

@Component({
  selector: 'app-pokemon-grid-item',
  templateUrl: './pokemon-grid-item.component.html',
  styleUrls: ['./pokemon-grid-item.component.css']
})
export class PokemonGridItemComponent implements OnInit {
  @Input() pokemon: PokemonShort;

  constructor() { }

  ngOnInit() { }

}
