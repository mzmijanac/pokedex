import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'; // Ajde da ovdje probamo taj novi HttpClientModule
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

// Pošto je ovo GET-only API, ovdje će biti jako puno shortcuta :D
// Inače bi za search radili POST request, itd...

@Injectable()
export class SearchService {
  private apiUrl: string = 'https://pokeapi.co/api/v2/pokemon';
  public spriteUrl: string = 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/';
  public limit: number = 811; // Limit je hardcoded, jer već znamo koliko ih ima, pa... fuck it, it's demo

  constructor(private http: HttpClient) {
  }

  // Mogao sam iskoristiti i fetch iz PokemonDataServicea
  public fetchAllPokemons(): Observable<string[]> {
    // Aww Yiss baybe, novi HttpClient ne treba json() na response
    return this.http.get<string[]>(`${this.apiUrl}?limit=${this.limit}`);
  }

  // Isti kurac kao u PokemonDataService
  public firstLetterToUppercase(string: string): string {
    return string.charAt(0).toUpperCase() + string.slice(1);
  }

}
