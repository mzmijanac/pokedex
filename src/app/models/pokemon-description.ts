import { PokemonTypes } from '../models/pokemon-types';

export class PokemonDescription {
  id: number;
  name: string;
  sprite: string;
  height?: number;
  weight?: number;
  infoText?: string;
  color?: string;
  type?: PokemonTypes[];

  constructor(
    id: number,
    name: string,
    sprite: string,
    height?: number,
    weight?: number,
    infoText?: string,
    color?: string,
    type?: PokemonTypes[]
  ){
    this.id = id;
    this.name = name;
    this.sprite = sprite;
    this.height = height;
    this.weight = weight;
    this.infoText = infoText;
    this.color = color;
    this.type = type;
  }

}
