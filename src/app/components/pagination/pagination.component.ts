import { Component, OnInit, OnChanges, Input, Output, EventEmitter } from '@angular/core';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.css']
})

/* Thinking out loud, na ~800 pokemona i offset od ~20, imamo oko 40 pageova... najbolje pokazati current page i previous/next,
možda eventualno 2-3 prije i poslije. Mislio sam prvo implementati nekakav infinite scroll, ali mrzim onaj feeling kad ne znaš koliko toga ima,
a kad sam skužio da ih fakat ima puno, nekak je bolje prikazati pagination pa da se zna točno koliko imaš do kraja i da možeš jumpati na last page.
*/

export class PaginationComponent implements OnInit, OnChanges {
  // Ajmo stavit nekakve defaultne config vrijednosti, ioako ćemo ih povlačiti iz PokemonServicea
  @Input() offset: number = 0;
  @Input() limit: number = 1; // Količina Pokemona na current pageu
  @Input() size: number = 1; // Ukupna količina pokemona
  @Input() range: number = 2; // Broj pageova prije / poslije current pagea
  @Output() pageChange: EventEmitter<number> = new EventEmitter<number>();

  currentPage: number;
  totalPages: number;
  pages: Observable<number[]>;

  constructor() { }

  ngOnInit() {
    this.getPages(this.offset, this.limit, this.size);
  }

  ngOnChanges(){
    this.getPages(this.offset, this.limit, this.size);
  }

  isValidPageNumber(page: number, totalPages: number): boolean {
    return page > 0 && page <= totalPages;
  }


  /* Ovdje se rađaju brojevi za pagination, npr. ak je range 3 da ima ukupno 7 pageova prikazano;
     ako je currentPage = 10, onda će biti prikazani pageovi 7, 8, 9, 10, 11, 12, 13, bejzikli this.range * 2 + 1,
     a na samom početku je -this.range da izbjegnemo -3, -2, -1 dok je currentPage = 0

     http://reactivex.io/documentation/operators/range.html
  */

  getPages(offset: number, limit: number, size: number){
    this.currentPage = this.getCurrentPage(offset, limit);
    this.totalPages = this.getTotalPages(limit, size);
    this.pages = Observable.range(-this.range, this.range * 2 + 1)
                           .map(offset => this.currentPage + offset)
                           .filter(page => this.isValidPageNumber(page, this.totalPages))
                           .toArray();
  }

  // Računam currentPage na temelju offseta i limita, te ukupni broj pageova na temelju limita i sizea.
  getCurrentPage(offset: number, limit: number): number {
    return Math.floor(offset / limit) + 1;
  }

  getTotalPages(limit: number, size: number): number {
    return Math.ceil(Math.max(size, 1) / Math.max(limit, 1));
  }

  selectPage(page: number, event){
    this.cancelEvent(event);
    if (this.isValidPageNumber(page, this.totalPages)){
      this.pageChange.emit((page - 1) * this.limit);
    }
  }

  // Odvojeno u drugu funkciju zbog stylinga paginationa, da moš callat directly zbog bindanja classa
  cancelEvent(event){
    event.preventDefault();
  }



}
