# Pokedex

Simple Angular@4.3.1 demo project.

![Pokedex](http://i.imgur.com/Xydg3kh.png)

## Prototyping sketches

![List Layout](http://i.imgur.com/Ylf2JJm.jpg)
![Single Layout](http://i.imgur.com/ulgLPqz.jpg)
![Sitemap](http://i.imgur.com/VWHEtGX.jpg)
![Scaffolding](http://i.imgur.com/MRZVxOh.jpg)
![Pokemon Model](http://i.imgur.com/FJw61xP.jpg)
![Api & Services](http://i.imgur.com/c9seUAy.jpg)
![Pagination & Search](http://i.imgur.com/mqQSTz7.jpg)

## Redneck Time Tracking

* Learning about Pokedex, Pokemons & Pokeapi: 3-5 hours
* Basic App prototyping: 1-2 hours
* Initial App scaffolding & Data models: 2-3 hours
* API services: 2-3 hours
* Components: 2-3 hours
* UI: 2 hours

## Known Issues

* Depending on API response, Search sometimes doesn't work immediately...

## ToDo's

* Fix known issues
* Lazy Loading
* Service Workers
* PWA
* Create separate API request service
* Implement HttpClientModule
* Update UI dramatically (implement `:host` styles)
* Add tests


## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

Run `ng serve --host=0.0.0.0 ` for local network testing on other devices. If you encounter "Invalid Host Header" error, add `--disable-host-check`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

